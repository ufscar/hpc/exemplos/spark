# Spark + Slurm + Singularity

![alt text](https://i.imgur.com/fBa438T.jpg)

You can use the image with:
`singularity exec --bind $PATH_TO_LOG:/opt/apache-spark/logs spark.sif -app master`

And connect in master with:

`singularity exec --bind $PWD:/opt/apache-spark/logs spark.sif -app slave $MASTER_IP`

